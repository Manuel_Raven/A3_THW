
		thrustDelay            	= 0.5; 		
		brakeIdleSpeed         	= 2.78; 	
		maxSpeed               	= 110; 		

		fuelCapacity           	= 355; 
		wheelCircumference     	= 3.776; 	
		antiRollbarForceCoef	= 0; 	
		antiRollbarForceLimit	= 5; 	
		antiRollbarSpeedMin 	= 0; 	
		antiRollbarSpeedMax		= 20;  	
		idleRpm = 600; 
		redRpm =2400;
		class complexGearbox {
			GearboxRatios[] = {"R1", -8.9700003, "N", 0, "D1", 9.4799995, "D2", 6.5799999, "D3", 4.6799998, "D4", 3.48, "D5", 2.6199999, "D6", 1.89, "D7", 1.35,"D8", 1, "D9", 0.75};
			TransmissionRatios[] = {"High", 6.5300002};
			gearBoxMode = "full_auto";
			moveOffGear = 1;
			driveString = "D";
			neutralString = "N";
			reverseString = "R";
		};		

		simulation = "carx";
		
		dampersBumpCoef = 6;
		
		differentialType = "all_limited";
		
		
		frontRearSplit = 0.5;
			
		frontBias = 1.3;
			
		rearBias = 1.3;
			
		centreBias = 1.3;
			
		clutchStrength = 55;
			
		enginePower = 551; 
		
		maxOmega = 340;
		
		peakTorque = 2280;
		
		
		dampingRateFullThrottle=0.079999998;
		dampingRateZeroThrottleClutchEngaged=2;
		dampingRateZeroThrottleClutchDisengaged=0.34999999;	
		
		torqueCurve[]= {{0,0.2}, {0.278,0.5}, {0.34999999,0.85000002}, {0.461,0.94999999}, {0.60000002,1}, {0.69999999,0.94999999}, {0.80000001,0.69999999}, {1,0.40000001}};
		
		changeGearMinEffectivity[] = {0.95, 0.15, 0.98, 0.98, 0.98, 0.98, 0.97, 0.95, 0.95};
		
		
		switchTime = 0.31;
		

		
		latency = 1;
			
		class Wheels
		{
			class LF
			{
				boneName="wheel_1_1_damper";
				steering= 1;
				side="left";
				center="wheel_1_1_axis";
				boundary="wheel_1_1_bound";
				width = 0.2;
				mass=20;
				MOI=12.3;
				dampingRate = 0.5;
				maxBrakeTorque=15000;
				maxHandBrakeTorque=12000;
				suspTravelDirection[]={0,-1,0};
				suspForceAppPointOffset="wheel_1_1_axis";
				tireForceAppPointOffset="wheel_1_1_axis";
				maxCompression=0.15000001;
				mMaxDroop=0.15000001;
				sprungMass=431;
				springStrength=48781;
				springDamperRate=12724;
				longitudinalStiffnessPerUnitGravity=4800;
				latStiffX=25;
				latStiffY=220;
				frictionVsSlipGraph[]= {{0.17,0.94999999}, {0.40000001,0.85000002}, {1,0.75}};
			};
			class LR: LF
			{
				boneName = "wheel_1_2_damper";
				steering = 0;
				center   = "wheel_1_2_axis";
				boundary = "wheel_1_2_bound";
				suspForceAppPointOffset = "wheel_1_2_axis";
				tireForceAppPointOffset = "wheel_1_2_axis";
				maxHandBrakeTorque = 3000;				
            };
            
            class LRR: LF
			{
				boneName = "wheel_1_3_damper";
				steering = 0;
				center   = "wheel_1_3_axis";
				boundary = "wheel_1_3_bound";
				suspForceAppPointOffset = "wheel_1_3_axis";
				tireForceAppPointOffset = "wheel_1_3_axis";
				maxHandBrakeTorque = 3000;				
			};

			class RF: LF
			{
				boneName = "wheel_2_1_damper";
				center   = "wheel_2_1_axis";
				boundary = "wheel_2_1_bound";
				suspForceAppPointOffset = "wheel_2_1_axis";
				tireForceAppPointOffset = "wheel_2_1_axis";
				steering = 1;
			};

			class RR: RF
			{
				boneName = "wheel_2_2_damper";
				steering = 0;
				center   = "wheel_2_2_axis";
				boundary = "wheel_2_2_bound";
				suspForceAppPointOffset = "wheel_2_2_axis";
				tireForceAppPointOffset = "wheel_2_2_axis";
				maxHandBrakeTorque = 3000;				
            };
            
            class RRR: RF
			{
				boneName = "wheel_2_3_damper";
				steering = 0;
				center   = "wheel_2_3_axis";
				boundary = "wheel_2_3_bound";
				suspForceAppPointOffset = "wheel_2_3_axis";
				tireForceAppPointOffset = "wheel_2_3_axis";
				maxHandBrakeTorque = 3000;				
			};
		};