enum {
	DESTRUCTENGINE = 2,
	DESTRUCTDEFAULT = 6,
	DESTRUCTWRECK = 7,
	DESTRUCTTREE = 3,
	DESTRUCTTENT = 4,
	STABILIZEDINAXISX = 1,
	STABILIZEDINAXESXYZ = 4,
	STABILIZEDINAXISY = 2,
	STABILIZEDINAXESBOTH = 3,
	DESTRUCTNO = 0,
	STABILIZEDINAXESNONE = 0,
	DESTRUCTMAN = 5,
	DESTRUCTBUILDING = 1,
};

#define private		0
#define protected		1
#define public		2

#define true	1
#define false	0

#define TEast		0
#define TWest		1
#define TGuerrila		2
#define TCivilian		3
#define TSideUnknown		4
#define TEnemy		5
#define TFriendly		6
#define TLogic		7

#include "basicdefines_A3.hpp"

class DefaultEventhandlers;	// External class reference

class CfgPatches {
	class DAR_MTVR_THW {
		units[] = {DAR_MK27_THW, DAR_4x4_THW};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Soft_F"};
	};
};

class CfgVehicleClasses {
	class DARMil_Vehicles {
		displayName = "DAR MTVR THW";
	};
};

class WeaponFireGun;	
class WeaponCloudsGun;	
class WeaponFireMGun;	
class WeaponCloudsMGun;	
class RCWSOptics;	

class CfgMovesBasic {
	class DefaultDie;
	
	class ManActions {};
};

class CfgMovesMaleSdr : CfgMovesBasic {
	
	class States {
	
		class Crew;
		class AmovPercMstpSnonWnonDnon;
		
		class HMMWV_KIA_Cargo01_EP1 : DefaultDie {
			actions = "DeadActions";
			file = "\DAR_MTVR_THW\Anim\HMWVW_KIA_Cargo01";
			speed = 1e+010;
			terminal = true;
			soundEnabled = false;
			looped = false;
			connectTo[] = {"Unconscious", 0.1};
		};
	};
};


class CfgVehicles {
	class LandVehicle;
	
	class Car : LandVehicle {
		class NewTurret;	
	};
	
	class Car_F : Car {
		class AnimationSources;	
		
		class HitPoints {
			class HitLFWheel;	
			class HitLF2Wheel;	
			class HitRFWheel;	
			class HitRF2Wheel;	
			class HitGlass1;	
			class HitGlass2;	
			class HitGlass3;	
			class HitGlass4;	
			class HitGlass5;	
			class HitGlass6;	
		};
	};
	
	class DAR_MTVR_THWBase : Car_F {
		class SpeechVariants {
			class Default {
				speechSingular[] = {"veh_vehicle_armedcar_s"};
				speechPlural[] = {"veh_vehicle_armedcar_p"};
			};
		};
		maxSpeed = 120;
		acceleration = 15;
		textSingular = "$STR_A3_nameSound_veh_vehicle_armedcar_s";
		textPlural = "$STR_A3_nameSound_veh_vehicle_armedcar_p";
		nameSound = "veh_vehicle_armedcar_s";
		author = "Richards.D, Bohemia Interactive";
		_generalMacro = "";
		scope = 0; 
		Model = "\DAR_MTVR_THW\DAR_MK27_THW.p3d";
		picture = "\DAR_MTVR_THW\Data\UI\picture_mtvr_ca.paa";
		Icon = "\DAR_MTVR_THW\Data\UI\icon_mtvr_ca.paa";
		mapSize = 6;
		displayName = MTVR Base;
		vehicleClass = "DARMil_Vehicles";
		crew = "B_Soldier_F";
		typicalCargo[] = {"B_Soldier_F", "B_Soldier_F", "B_Soldier_F", "B_Soldier_F"};
		side = TWest;
		faction = BLU_F;
		wheelCircumference = 4.08407044; 	
		antiRollbarForceCoef = 1.5;
		antiRollbarForceLimit = 4;
		antiRollbarSpeedMin = 0;
		antiRollbarSpeedMax = 20;
		driverCompartments = "Compartment1";
		cargoCompartments[] = {"Compartment1", "Compartment2"};
		soundAttenuationCargo[] = {1, 0};
		crewVulnerable = true;
		ejectDeadCargo = false;
		ejectDeadDriver = true;
		crewCrashProtection = 0.8;
		weapons[] = {"TruckHorn2"};
		magazines[] = {};
		damperSize = 0.2;	// max. damper amplitude
		damperForce = 1;	// larger number more stiffness dampers
		damperDamping = 1;	// larger number less movement in dampers
		armor = 80;
		damageResistance = 0.05;
		turnCoef = 3;
		steerAheadPlan = 0.2;
		steerAheadSimul = 0.4;
		predictTurnPlan = 0.9;
		predictTurnSimul = 0.5;
		brakeDistance = 1.0;	// vehicle movement precision
		terrainCoef = 2.5;
		wheelDamageThreshold = 0.9;
		wheelDestroyThreshold = 0.99;
		wheelDamageRadiusCoef = 0.95;
		wheelDestroyRadiusCoef = 0.45;
		cost = 200000;
		precision = 15;
		armorGlass = 0.9;
		armorWheels = 0.8;
		soundServo[] = {"A3\sounds_f\dummysound", db-40, 1.0, 10};
		soundEnviron[] = {"", 0.562341, 1};
		transportMaxBackpacks = 50;
		transportSoldier = 13;
		
		class Library {
			libTextDesc = "DAR MTVRs";
		};
		castDriverShadow = "false";
		driverAction = "driver_offroad01";
		cargoAction[] = {"passenger_low01", "passenger_generic01_leanright", "passenger_generic01_foldhands", "passenger_generic01_foldhands", "passenger_generic01_foldhands"};
		
		// threat (VSoft, VArmor, VAir), how threatening vehicle is to unit types
		threat[] = {0.8, 0.6, 0.3};

		driverLeftHandAnimName = "drivewheel";
		driverRightHandAnimName = "drivewheel";
		
		class TransportMagazines {};
		
		class TransportItems {};
		
		class TransportWeapons {};

		class VehicleTransport
		{
			class Carrier
			{
				cargoBayDimensions[]        = {"VTV_limit_1", "VTV_limit_2"};   // Memory points in model defining cargo space
				disableHeightLimit          = 1;                             // If set to 1 disable height limit of transported vehicles
				maxLoadMass                 = 200000;                           // Maximum cargo weight (in Kg) which the vehicle can transport
				cargoAlignment[]            = {"center", "front"};                // Array of 2 elements defining alignment of vehicles in cargo space. Possible values are left, right, center, front, back. Order is important.
				cargoSpacing[]              = {0, 0.15, 0};                     // Offset from X,Y,Z axes (in metres)
				exits[]                     = {"VTV_exit_1"};     // Memory points in model defining loading ramps, could have multiple
				unloadingInterval           = 1;                                // Time between unloading vehicles (in seconds)
				loadingDistance             = 10;                               // Maximal distance for loading in exit point (in meters).
				loadingAngle                = 60;                               // Maximal sector where cargo vehicle must be to for loading (in degrees).
				parachuteClassDefault       = B_Parachute_02_F;                 // Type of parachute used when dropped in air. Can be overridden by parachuteClass in Cargo.
				parachuteHeightLimitDefault = 50;                               // Minimal height above terrain when parachute is used. Can be overriden by parachuteHeightLimit in Cargo.
			};
		};
		
		idleRpm = 800;
		redRpm = 5500;
		brakeIdleSpeed = 1.78;
		fuelCapacity = 250;
		
		class complexGearbox {
			GearboxRatios[] = {"R1", -2.575, "N", 0, "D1", 4.5, "D2", 2.6, "D3", 1.32, "D4", 0.96, "D5", 0.65};
			TransmissionRatios[] = {"High", 5.539};
			gearBoxMode = "auto";
			moveOffGear = 1;
			driveString = "D";
			neutralString = "N";
			reverseString = "R";
		};
		simulation = "carx";
		dampersBumpCoef = 6.0;
		differentialType = "all_limited";
		frontRearSplit = 0.5;
		frontBias = 1.3;
		rearBias = 1.3;
		centreBias = 1.3;
		clutchStrength = 55.0;
		enginePower = 650;
		maxOmega = 900.91;
		peakTorque = 1253;
		dampingRateFullThrottle = 0.08;
		dampingRateZeroThrottleClutchEngaged = 2.0;
		dampingRateZeroThrottleClutchDisengaged = 0.35;
		torqueCurve[] = {{0, 0}, {0.14, 0.41}, {0.29, 0.77}, {0.43, 1}, {0.57, 1}, {0.71, 0.87}, {0.86, 0.77}, {1, 0.62}};
		changeGearMinEffectivity[] = {0.95, 0.15, 0.95, 0.95, 0.95, 0.95, 0.95};
		switchTime = 0.31;
		latency = 1.0;
		
		class Wheels
		{
			class LF
			{
				boneName = "wheel_1_1_damper";
				steering = 1;
				side = "left";
				center   = "wheel_1_1_axis";
				boundary = "wheel_1_1_bound";
				mass = 20;
				MOI = 3.3;
				dampingRate = 0.5;
				maxBrakeTorque = 6000;
				maxHandBrakeTorque = 0;
				suspTravelDirection[] = {0, -1, 0};
				suspForceAppPointOffset = "wheel_1_1_axis";
				tireForceAppPointOffset = "wheel_1_1_axis";
				maxCompression = 0.15;
				mMaxDroop = 0.15;
				sprungMass = 550;
				springStrength = 50875;
				springDamperRate = 12740;
				longitudinalStiffnessPerUnitGravity = 8000;
				latStiffX = 35;
				latStiffY = 180;
				frictionVsSlipGraph[] = {{0.17, 0.85}, {0.4, 0.65}, {1, 0.4}};
			};
			class LR : LF
			{
				boneName = "wheel_1_2_damper";
				steering = 0;
				center   = "wheel_1_2_axis";
				boundary = "wheel_1_2_bound";
				suspForceAppPointOffset = "wheel_1_2_axis";
				tireForceAppPointOffset = "wheel_1_2_axis";
				maxHandBrakeTorque = 3000;
				latStiffY = 180;	
				sprungMass = 550;
				springStrength = 50875;
				springDamperRate = 12740;				
			};
			
			class LR1 : LR
			{
				boneName = "wheel_1_3_damper";
				steering = 0;
				center   = "wheel_1_3_axis";
				boundary = "wheel_1_3_bound";
				suspForceAppPointOffset = "wheel_1_3_axis";
				tireForceAppPointOffset = "wheel_1_3_axis";
				maxHandBrakeTorque = 3000;
				latStiffY = 180;	
				sprungMass = 550;
				springStrength = 50875;
				springDamperRate = 12740;				
			};
			
			class RF : LF
			{
				boneName = "wheel_2_1_damper";
				center   = "wheel_2_1_axis";
				boundary = "wheel_2_1_bound";
				suspForceAppPointOffset = "wheel_2_1_axis";
				tireForceAppPointOffset = "wheel_2_1_axis";
				steering = 1;
				side = "right";
			};
			class RR : RF
			{
				boneName = "wheel_2_2_damper";
				steering = 0;
				center   = "wheel_2_2_axis";
				boundary = "wheel_2_2_bound";
				suspForceAppPointOffset = "wheel_2_2_axis";
				tireForceAppPointOffset = "wheel_2_2_axis";
				maxHandBrakeTorque = 3000;
				latStiffY = 180;	
				sprungMass = 550;
				springStrength = 50875;
				springDamperRate = 12740;			
			};
			
			class RR1 : RR
			{
				boneName = "wheel_2_3_damper";
				steering = 0;
				center   = "wheel_2_3_axis";
				boundary = "wheel_2_3_bound";
				suspForceAppPointOffset = "wheel_2_3_axis";
				tireForceAppPointOffset = "wheel_2_3_axis";
				maxHandBrakeTorque = 3000;
				latStiffY = 180;	
				sprungMass = 550;
				springStrength = 50875;
				springDamperRate = 12740;			
			};

		};
		
		#include "sounds.hpp"
		#include "physx.hpp"	/// PhysX settings are in a separate file to make this one simple

		class Exhausts {
			class Exhaust1 {
				position = "vyfuk start";
				direction = "vyfuk konec";
				effect = "ExhaustEffectOffroad";
			};
		};
		
		class HitPoints : HitPoints {
			class HitGlass1 : HitGlass1 {
				armor = 0.1;
			};
			
			class HitGlass2 : HitGlass2 {
				armor = 0.1;
			};
			
			class HitGlass3 : HitGlass3 {
				armor = 0.1;
			};
			
			class HitGlass4 : HitGlass4 {
				armor = 0.1;
			};
			
			class HitLFWheel : HitLFWheel {
				armor = 0.38;
			};
			
			class HitLBWheel : HitLF2Wheel {
				armor = 0.38;
			};
			
			class HitRFWheel : HitRFWheel {
				armor = 0.38;
			};
			
			class HitRBWheel : HitRF2Wheel {
				armor = 0.38;
			};
			
			class HitFuel {
				armor = 1.4;
				material = -1;
				name = "palivo";
				visual = "";
				passThrough = true;
			};
		};
		
		class Reflectors {
			class LightCarHeadL01 {
				color[] = {1900, 1800, 1700};
				ambient[] = {5, 5, 5};
				position = "LightCarHeadL01";
				direction = "LightCarHeadL01_end";
				hitpoint = "Light_L";
				selection = "Light_L";
				size = 1;
				innerAngle = 100;
				outerAngle = 179;
				coneFadeCoef = 10;
				intensity = 1;
				useFlare = 1;
				dayLight = 0;
				flareSize = 1;
				
				class Attenuation {
					start = 1;
					constant = 0;
					linear = 0;
					quadratic = 0.25;
					hardLimitStart = 30;
					hardLimitEnd = 60;
				};
			};
			
			class LightCarHeadL02 : LightCarHeadL01 {
				position = "LightCarHeadL02";
				direction = "LightCarHeadL02_end";
				FlareSize = 0.5;
			};
			
			class LightCarHeadR01 : LightCarHeadL01 {
				position = "LightCarHeadR01";
				direction = "LightCarHeadR01_end";
				hitpoint = "Light_R";
				selection = "Light_R";
			};
			
			class LightCarHeadR02 : LightCarHeadR01 {
				position = "LightCarHeadR02";
				direction = "LightCarHeadR02_end";
				FlareSize = 0.5;
			};
		};
		aggregateReflectors[] = {{"Left", "Right", "Left2", "Right2"}};
		class AnimationSources: AnimationSources	/// hides all the proxies and beacons, to be sure
		{
			class Beacons
			{
				source		 = "user";
				animPeriod	 = 1;
				initPhase	 = 0;
			};
		};
		class RenderTargets {
			class LeftMirror {
				renderTarget = "rendertarget0";
				
				class CameraView1 {
					pointPosition = "PIP0_pos";
					pointDirection = "PIP0_dir";
					renderQuality = 2;
					renderVisionMode = 4;
					fov = 0.7;
				};
			};
			
			class RightMirror {
				renderTarget = "rendertarget2";
				
				class CameraView1 {
					pointPosition = "PIP2_pos";
					pointDirection = "PIP2_dir";
					renderQuality = 2;
					renderVisionMode = 4;
					fov = 0.7;
				};
			};
		};
	};
	
	
	class DAR_MK27_THW : DAR_MTVR_THWBase {
		scope = 2;
		model = "\DAR_MTVR_THW\DAR_MK27_THW.p3d";
		displayname = "Mk. 27 MTVR Extended Cargo";
		transportSoldier = 28;
		transportMaxBackpacks = 50;
		picture = "\DAR_MTVR_THW\Data\UI\picture_mtvr_ca.paa";
		Icon = "\DAR_MTVR_THW\Data\UI\icon_mtvr_ca.paa";
		hasGunner = false;
		
		side = TWest;
		faction = BLU_F;
		
		class Library {
			libTextDesc = "Mk. 27 MTVR Extended";
		};

		vehicleClass = "DARMil_Vehicles";
		
		// threat (VSoft, VArmor, VAir), how threatening vehicle is to unit types
		threat[] = {0.0, 0.0, 0.0};

		
		class TransportWeapons {};
		class UserActions /// Adding possibility for user to switch the beacons on/off
		{
			class beacons_start
			{
				userActionID 		 = 50;								/// just some unique number
				displayName 		 = $STR_A3_CfgVehicles_beacons_on;	/// what is displayed in the action menu
				displayNameDefault 	 = $STR_A3_CfgVehicles_beacons_on;	/// what is displayed in middle of screen
				position			 = "drivewheel";						/// at what memory point of the ship is the used as center of the radius
				priority 			 = 1.5;								/// sorting of action menu
				radius				 = 1.8;								/// radius around position where the action is avaliable
				animPeriod			 = 2;								/// how long does the animation source take to go from 0 to 1
				onlyForPlayer		 = false;							/// it is usable even by AI
				condition			 = "this animationPhase ""BeaconsStart"" < 0.5 AND Alive(this) AND driver this == player"; /// at what condition is the action displayed
				statement			 = "this animate [""BeaconsStart"",1];"; /// and what happens when the action is used
			};

			class beacons_stop: beacons_start
			{
				userActionID 		 = 51;
				displayName 		 = $STR_A3_CfgVehicles_beacons_off;
				displayNameDefault 	 = $STR_A3_CfgVehicles_beacons_off;
				condition			 = "this animationPhase ""BeaconsStart"" > 0.5 AND Alive(this) AND driver this == player";
				statement			 = "this animate [""BeaconsStart"",0];";
			};
		};
	};
	
	
	class DAR_4X4_THW : DAR_MTVR_THWBase {
		scope = 2;
		model = "\DAR_MTVR_THW\DAR_4X4_THW.p3d";
		displayname = "MTVR 4X4 Short Bed";
		transportSoldier = 7;
		transportMaxBackpacks = 25;
		picture = "\DAR_MTVR_THW\Data\UI\picture_mtvr_ca.paa";
		Icon = "\DAR_MTVR_THW\Data\UI\icon_mtvr_ca.paa";
		hasGunner = false;
		
		side = TWest;
		faction = BLU_F;
		
		class Wheels : Wheels
		{
			class LF
			{
				boneName = "wheel_1_1_damper";
				steering = 1;
				side = "left";
				center   = "wheel_1_1_axis";
				boundary = "wheel_1_1_bound";
				mass = 20;
				MOI = 3.3;
				dampingRate = 0.5;
				maxBrakeTorque = 6000;
				maxHandBrakeTorque = 0;
				suspTravelDirection[] = {0, -1, 0};
				suspForceAppPointOffset = "wheel_1_1_axis";
				tireForceAppPointOffset = "wheel_1_1_axis";
				maxCompression = 0.15;
				mMaxDroop = 0.15;
				sprungMass = 550;
				springStrength = 50875;
				springDamperRate = 12740;
				longitudinalStiffnessPerUnitGravity = 8000;
				latStiffX = 35;
				latStiffY = 180;
				frictionVsSlipGraph[] = {{0.17, 0.85}, {0.4, 0.65}, {1, 0.4}};
			};
			class LR : LF
			{
				boneName = "wheel_1_2_damper";
				steering = 0;
				center   = "wheel_1_2_axis";
				boundary = "wheel_1_2_bound";
				suspForceAppPointOffset = "wheel_1_2_axis";
				tireForceAppPointOffset = "wheel_1_2_axis";
				maxHandBrakeTorque = 3000;
				latStiffY = 180;	
				sprungMass = 550;
				springStrength = 50875;
				springDamperRate = 12740;				
			};
			
			class RF : LF
			{
				boneName = "wheel_2_1_damper";
				center   = "wheel_2_1_axis";
				boundary = "wheel_2_1_bound";
				suspForceAppPointOffset = "wheel_2_1_axis";
				tireForceAppPointOffset = "wheel_2_1_axis";
				steering = 1;
				side = "right";
			};
			class RR : RF
			{
				boneName = "wheel_2_2_damper";
				steering = 0;
				center   = "wheel_2_2_axis";
				boundary = "wheel_2_2_bound";
				suspForceAppPointOffset = "wheel_2_2_axis";
				tireForceAppPointOffset = "wheel_2_2_axis";
				maxHandBrakeTorque = 3000;
				latStiffY = 180;	
				sprungMass = 550;
				springStrength = 50875;
				springDamperRate = 12740;			
			};
		};

		vehicleClass = "DARMil_Vehicles";
		
		// threat (VSoft, VArmor, VAir), how threatening vehicle is to unit types
		threat[] = {0.0, 0.0, 0.0};

		
		class TransportWeapons {};

		
		class TransportWeapons {};
		class UserActions /// Adding possibility for user to switch the beacons on/off
		{
			class beacons_start
			{
				userActionID 		 = 50;								/// just some unique number
				displayName 		 = $STR_A3_CfgVehicles_beacons_on;	/// what is displayed in the action menu
				displayNameDefault 	 = $STR_A3_CfgVehicles_beacons_on;	/// what is displayed in middle of screen
				position			 = "drivewheel";						/// at what memory point of the ship is the used as center of the radius
				priority 			 = 1.5;								/// sorting of action menu
				radius				 = 1.8;								/// radius around position where the action is avaliable
				animPeriod			 = 2;								/// how long does the animation source take to go from 0 to 1
				onlyForPlayer		 = false;							/// it is usable even by AI
				condition			 = "this animationPhase ""BeaconsStart"" < 0.5 AND Alive(this) AND driver this == player"; /// at what condition is the action displayed
				statement			 = "this animate [""BeaconsStart"",1];"; /// and what happens when the action is used
			};

			class beacons_stop: beacons_start
			{
				userActionID 		 = 51;
				displayName 		 = $STR_A3_CfgVehicles_beacons_off;
				displayNameDefault 	 = $STR_A3_CfgVehicles_beacons_off;
				condition			 = "this animationPhase ""BeaconsStart"" > 0.5 AND Alive(this) AND driver this == player";
				statement			 = "this animate [""BeaconsStart"",0];";
			};
		};
	};
};
