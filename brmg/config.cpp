#include "basicdefines_A3.hpp"
class DefaultEventhandlers;

#include "CfgPatches.hpp"

class WeaponFireGun;
class WeaponCloudsGun;
class WeaponFireMGun;
class WeaponCloudsMGun;

class CfgVehicles
{
	class Truck_F;
	class Car_F: Truck_F
	{
		class HitPoints /// we want to use hitpoints predefined for all cars
		{
			class HitLFWheel;
			class HitLF2Wheel;
			class HitRFWheel;
			class HitRF2Wheel;
			class HitBody;
			class HitGlass1;
			class HitGlass2;
			class HitGlass3;
			class HitGlass4;
		};
		class EventHandlers;
		class AnimationSources;
	};

	class MNLRVN_brmg_BASE_F: Car_F
	{
		model 	= "\brmg\brmg";  /// simple path to model
		picture	= "\A3\Weapons_F\Data\placeholder_co.paa"; /// just some icon in command bar
		Icon	= "\A3\Weapons_F\Data\placeholder_co.paa"; /// icon in map
		selectionBrakeLights = "breake_lights";
		selectionBackLights = "back_lights";
		displayName = "THW brmg1"; /// displayed in Editor

		terrainCoef 	= 2.0; 	/// different surface affects this car more, stick to tarmac
		turnCoef 		= 2.5; 	/// should match the wheel turn radius
		precision 		= 10; 	/// how much freedom has the AI for its internal waypoints - lower number means more precise but slower approach to way
		brakeDistance 	= 3.0; 	/// how many internal waypoints should the AI plan braking in advance
		acceleration 	= 15; 	/// how fast acceleration does the AI think the car has

		fireResistance 	= 5; 	/// lesser protection against fire than tanks
		armor 			= 120; 	/// just some protection against missiles, collisions and explosions
		cost			= 50000; /// how likely is the enemy going to target this vehicle

		weapons[]=
		{
			"TruckHorn3"
		};

		transportMaxBackpacks 	= 3; /// just some backpacks fit the trunk by default
		transportSoldier 		= 3; /// number of cargo except driver

		/// some values from parent class to show how to set them up
		wheelDamageRadiusCoef 	= 0.9; 			/// for precision tweaking of damaged wheel size
		wheelDestroyRadiusCoef 	= 0.4;			/// for tweaking of rims size to fit ground
		maxFordingDepth 		= 0.5;			/// how high water would damage the engine of the car
		waterResistance 		= 1;			/// if the depth of water is bigger than maxFordingDepth it starts to damage the engine after this time
		crewCrashProtection		= 0.25;			/// multiplier of damage to crew of the vehicle => low number means better protection
		class TransportItems /// some first aid kits in trunk according to safety regulations
		{
			item_xx(FirstAidKit,4);
		};

		class Turrets{}; /// doesn't have any gunner nor commander
		class HitPoints: HitPoints
		{
			class HitLFWheel: HitLFWheel	{armor=0.125; passThrough=0;}; /// it is easier to destroy wheels than hull of the vehicle
			class HitLF2Wheel: HitLF2Wheel	{armor=0.125; passThrough=0;};

			class HitRFWheel: HitRFWheel	{armor=0.125; passThrough=0;};
			class HitRF2Wheel: HitRF2Wheel 	{armor=0.125; passThrough=0;};

			class HitFuel 			{armor=0.50; material=-1; name="fueltank"; visual=""; passThrough=0.2;}; /// correct points for fuel tank, some of the damage is aFRLied to the whole
			class HitEngine 		{armor=0.50; material=-1; name="engine"; visual=""; passThrough=0.2;};
			class HitBody: HitBody 	{name = "body"; visual="camo1"; passThrough=1;}; /// all damage to the hull is aFRLied to total damage

			class HitGlass1: HitGlass1 {armor=0.25;}; /// it is pretty easy to puncture the glass but not so easy to remove it
			class HitGlass2: HitGlass2 {armor=0.25;};
			class HitGlass3: HitGlass3 {armor=0.25;};
			class HitGlass4: HitGlass4 {armor=0.25;};
		};

		driverAction 		= driver_offroad01; /// what action is going the driver take inside the vehicle. Non-existent action makes the vehicle inaccessible
		cargoAction[] 		= {passenger_low01, passenger_generic01_leanleft, passenger_generic01_foldhands}; /// the same of all the crew
		getInAction 		= GetInLow; 		/// how does driver look while getting in
		getOutAction 		= GetOutLow; 		/// and out
		cargoGetInAction[] 	= {"GetInLow"}; 	/// and the same for the rest, if the array has fewer members than the count of crew, the last one is used for the rest
		cargoGetOutAction[] = {"GetOutLow"}; 	/// that means all use the same in this case

		#include "sounds.hpp"	/// sounds are in a separate file to make this one simple
		#include "pip.hpp"		/// PiPs are in a separate file to make this one simple
		#include "physx.hpp"	/// PhysX settings are in a separate file to make this one simple

		class PlayerSteeringCoefficients /// steering sensitivity configuration
		{
			 turnIncreaseConst 	= 0.3; // basic sensitivity value, higher value = faster steering
			 turnIncreaseLinear = 1.0; // higher value means less sensitive steering in higher speed, more sensitive in lower speeds
			 turnIncreaseTime 	= 1.0; // higher value means smoother steering around the center and more sensitive when the actual steering angle gets closer to the max. steering angle

			 turnDecreaseConst 	= 5.0; // basic caster effect value, higher value = the faster the wheels align in the direction of travel
			 turnDecreaseLinear = 3.0; // higher value means faster wheel re-centering in higher speed, slower in lower speeds
			 turnDecreaseTime 	= 0.0; // higher value means stronger caster effect at the max. steering angle and weaker once the wheels are closer to centered position

			 maxTurnHundred 	= 0.7; // coefficient of the maximum turning angle @ 100km/h; limit goes linearly to the default max. turn. angle @ 0km/h
		};

		/// memory points where do tracks of the wheel appear
		// front left track, left offset
		memoryPointTrackFLL = "TrackFLL";
		// front left track, right offset
		memoryPointTrackFLR = "TrackFLR";
		// back left track, left offset
		memoryPointTrackBLL = "TrackBLL";
		// back left track, right offset
		memoryPointTrackBLR = "TrackBLR";
		// front right track, left offset
		memoryPointTrackFRL = "TrackFRL";
		// front right track, right offset
		memoryPointTrackFRR = "TrackFRR";
		// back right track, left offset
		memoryPointTrackBRL = "TrackBRL";
		// back right track, right offset
		memoryPointTrackBRR = "TrackBRR";

		class Damage /// damage changes material in specific places (visual in hitPoint)
		{
			tex[]={};
			mat[]=
			{
				"A3\data_f\glass_veh_int.rvmat", 		/// material mapped in model
				"A3\data_f\Glass_veh_damage.rvmat", 	/// changes to this one once damage of the part reaches 0.5
				"A3\data_f\Glass_veh_damage.rvmat",		/// changes to this one once damage of the part reaches 1

				"A3\data_f\glass_veh.rvmat",			/// another material
				"A3\data_f\Glass_veh_damage.rvmat",		/// changes into different ones
				"A3\data_f\Glass_veh_damage.rvmat"
			};
		};

		class Exhausts /// specific exhaust effects for the car
		{
			class Exhaust1 /// the car has two exhausts - each on one side
			{
				position 	= "exhaust";  		/// name of initial memory point
				direction 	= "exhaust_dir";	/// name of memory point for exhaust direction
				effect 		= "ExhaustsEffect";	/// what particle effect is it going to use
			};

			class Exhaust2
			{
				position 	= "exhaust2_pos";
				direction 	= "exhaust2_dir";
				effect 		= "ExhaustsEffect";
			};
		};

		class Reflectors	/// only front lights are considered to be reflectors to save CPU
		{
			class LightCarHeadL01 	/// lights on each side consist of two bulbs with different flares
			{
				color[] 		= {1900, 1800, 1700};		/// approximate colour of standard lights
				ambient[]		= {5, 5, 5};				/// nearly a white one
				position 		= "LightCarHeadL01";		/// memory point for start of the light and flare
				direction 		= "LightCarHeadL01_end";	/// memory point for the light direction
				hitpoint 		= "Light_L";				/// point(s) in hitpoint lod for the light (hitPoints are created by engine)
				selection 		= "Light_L";				/// selection for artificial glow around the bulb, not much used any more
				size 			= 1;						/// size of the light point seen from distance
				innerAngle 		= 100;						/// angle of full light
				outerAngle 		= 179;						/// angle of some light
				coneFadeCoef 	= 10;						/// attenuation of light between the above angles
				intensity 		= 1;						/// strength of the light
				useFlare 		= true;						/// does the light use flare?
				dayLight 		= false;					/// switching light off during day saves CPU a lot
				flareSize 		= 1.0;						/// how big is the flare

				class Attenuation
				{
					start 			= 1.0;
					constant 		= 0;
					linear 			= 0;
					quadratic 		= 0.25;
					hardLimitStart 	= 30;		/// it is good to have some limit otherwise the light would shine to infinite distance
					hardLimitEnd 	= 60;		/// this allows adding more lights into scene
				};
			};

			class LightCarHeadL02: LightCarHeadL01
			{
				position 	= "LightCarHeadL02";
				direction 	= "LightCarHeadL02_end";
				FlareSize 	= 0.5;						/// side bulbs aren't that strong
			};

			class LightCarHeadR01: LightCarHeadL01
			{
				position 	= "LightCarHeadR01";
				direction 	= "LightCarHeadR01_end";
				hitpoint 	= "Light_R";
				selection 	= "Light_R";
			};

			class LightCarHeadR02: LightCarHeadR01
			{
				position 	= "LightCarHeadR02";
				direction 	= "LightCarHeadR02_end";
				FlareSize 	= 0.5;
			};
		};

		aggregateReflectors[] = {{"LightCarHeadL01", "LightCarHeadL02"}, {"LightCarHeadR01", "LightCarHeadR02"}}; /// aggregating reflectors helps the engine a lot
		/// it might be even good to aggregate all lights into one source as it is done for most of the cars

		class EventHandlers: EventHandlers
		{
			// (_this select 0): the vehicle
			// """" Random texture source (pick one from the property textureList[])
			// []: randomize the animation sources (accordingly to the property animationList[])
			// false: Don't change the mass even if an animation source has a defined mass
			init="if (local (_this select 0)) then {[(_this select 0), """", [], false] call bis_fnc_initVehicle;};";
		};

		class AnimationSources: AnimationSources	/// hides all the proxies and beacons, to be sure
		{
			class Beacons
			{
				source		 = "user";
				animPeriod	 = 1;
				initPhase	 = 0;
			};
			class lifterRoatation
			{
				source = "user";
				initPhase = 0;
				animPeriod = 8;
			};
			class gabelRoatation
			{
				source = "user";
				initPhase = 0;
				animPeriod = 6;
			};
		};

		// Must be kept as fail-safe in case of issue with the function
		hiddenSelectionsTextures[]={"\A3\Weapons_F\Data\placeholder_co.paa"};	 /// we could use any texture to cover the car

		// Definition of texture sources (skins), used for the VhC (Vehicle customization)
		// Also, because the Garage uses the VhC, it will make them available from the garage
		class textureSources
		{
		};
		// [_textureSourceClass1, _probability1, _textureSourceClass2, _probability2, ...]
		// Default behavior of the VhC is to select one of these sources, with a weighted random
		textureList[]=
		{
			"red", 1,
			"green", __EVAL(1/3), // You can also use EVAL to evaluate an expression
			"blue", 1,
			"black", 1
			// You can noticed that the white source is missing, therefore, it won't be part of the random
		};

		class MFD /// Clocks on the car board
		{
			class ClockHUD
			{
				#include "cfgHUD.hpp"
			};
		};
		class UserActions /// Adding possibility for user to switch the beacons on/off
		{
			/*
			HeliTrimBackward => "Right Ctrl+2 [NUM]"
HeliTrimForward => "Right Ctrl+8 [NUM]"
HeliTrimLeft => "Right Ctrl+4 [NUM]"
HeliTrimRight => "Right Ctrl+6 [NUM]"
HeliTrimRudderLeft => "Right Ctrl+1 [NUM]"
HeliTrimRudderRight => "Right Ctrl+3 [NUM]"*/
			class beacons_start
			{
				userActionID 		 = 50;								/// just some unique number
				displayName 		 = $STR_A3_CfgVehicles_beacons_on;	/// what is displayed in the action menu
				displayNameDefault 	 = $STR_A3_CfgVehicles_beacons_on;	/// what is displayed in middle of screen
				position			 = "mph_axis";						/// at what memory point of the ship is the used as center of the radius
				priority 			 = 1.5;								/// sorting of action menu
				radius				 = 1.8;								/// radius around position where the action is avaliable
				animPeriod			 = 2;								/// how long does the animation source take to go from 0 to 1
				onlyForPlayer		 = false;							/// it is usable even by AI
				condition			 = "this animationPhase ""BeaconsStart"" < 0.5 AND Alive(this) AND driver this == player"; /// at what condition is the action displayed
				statement			 = "this animate [""BeaconsStart"",1];"; /// and what happens when the action is used
			};

			class beacons_stop: beacons_start
			{
				userActionID 		 = 51;
				displayName 		 = $STR_A3_CfgVehicles_beacons_off;
				displayNameDefault 	 = $STR_A3_CfgVehicles_beacons_off;
				condition			 = "this animationPhase ""BeaconsStart"" > 0.5 AND Alive(this) AND driver this == player";
				statement			 = "this animate [""BeaconsStart"",0];";
			};
			class lifterstop
			{
				userActionID 		 = 52;								/// just some unique number
				displayName 		 = "<t color='#ff0000'>Stoppen (R)</t>";	/// what is displayed in the action menu
				displayNameDefault 	 = "<t color='#ff0000'>Stoppen (R)</t>";	/// what is displayed in middle of screen						/// at what memory point of the ship is the used as center of the radius
				shortcut = "reloadMagazine";
				position			 = "mph_axis";	
				radius = 200;
				onlyForplayer = 1;					/// it is usable even by AI
				condition			 = "Alive(this) AND driver this == player"; /// at what condition is the action displayed
				statement			 = "this animate [""lifterRoatation"",this animationphase ""lifterRoatation""] && this animate [""gabelRoatation"",this animationphase ""gabelRoatation""]"; /// and what happens when the action is used
			};
			class lifterhoch
			{
				userActionID 		 = 53;								/// just some unique number
				displayName 		 = "<t color='#ff0000'>Hoch (Strg + NUM 8)</t>";	/// what is displayed in the action menu
				displayNameDefault 	 = "<t color='#ff0000'>Hoch (Strg + NUM 8)</t>";	/// what is displayed in middle of screen						/// at what memory point of the ship is the used as center of the radius
				shortcut = "HeliTrimForward";
				radius = 200;
				position			 = "mph_axis";	
				onlyForplayer = 1;					/// it is usable even by AI
				condition			 = "Alive(this) AND driver this == player"; /// at what condition is the action displayed
				statement			 = "this animate [""lifterRoatation"",1]"; /// and what happens when the action is used
			};
			class lifterrunter
			{
				userActionID 		 = 55;								/// just some unique number
				displayName 		 = "<t color='#ff0000'>Runter (Strg + NUM 2)</t>";	/// what is displayed in the action menu
				displayNameDefault 	 = "<t color='#ff0000'>Runter (Strg + NUM 2)</t>";	/// what is displayed in middle of screen				/// at what memory point of the ship is the used as center of the radius
				shortcut = "HeliTrimBackward";
				position			 = "mph_axis";	
				radius = 200;
				onlyForplayer = 1;					/// it is usable even by AI
				condition			 = "Alive(this) AND driver this == player"; /// at what condition is the action displayed
				statement			 = "this animate [""lifterRoatation"",0]"; /// and what happens when the action is used
			};
			class gabelhoch
			{
				userActionID 		 = 56;				
				position			 = "mph_axis";					/// just some unique number
				displayName 		 = "<t color='#ff0000'>Aufsatz Hoch (Strg + NUM 4)</t>";	/// what is displayed in the action menu
				displayNameDefault 	 = "<t color='#ff0000'>Aufsatz Hoch (Strg + NUM 4)</t>";	/// what is displayed in middle of screen						/// at what memory point of the ship is the used as center of the radius
				shortcut = "HeliTrimLeft";
				onlyForplayer = 1;
				radius = 200;					/// it is usable even by AI
				condition			 = "Alive(this) AND driver this == player"; /// at what condition is the action displayed
				statement			 = "this animate [""gabelRoatation"",0]"; /// and what happens when the action is used
			};
			class gabelrunter
			{
				userActionID 		 = 57;								/// just some unique number
				displayName 		 = "<t color='#ff0000'>Aufsatz Runter (Strg + NUM 6)</t>";	/// what is displayed in the action menu
				displayNameDefault 	 = "<t color='#ff0000'>Aufsatz Runter (Strg + NUM 6)</t>";	/// what is displayed in middle of screen						/// at what memory point of the ship is the used as center of the radius
				shortcut = "HeliTrimRight";
				position			 = "mph_axis";	
				onlyForplayer = 1;		
				radius = 200;			/// it is usable even by AI
				condition			 = "Alive(this) AND driver this == player"; /// at what condition is the action displayed
				statement			 = "this animate [""gabelRoatation"",1]"; /// and what happens when the action is used
			};
		};
	};
	// Derivate from the base class
	class C_MNLRVN_brmg: MNLRVN_brmg_BASE_F /// some class that is going to be visible in editor
	{
		scope	= 2; 			/// makes the car visible in editor
		scopeCurator=2;			// scope 2 means it's available in Zeus mode (0 means hidden)
		crew 	= "C_man_1"; 	/// we need someone to fit into the car
		side	= 3; 			/// civilian car should be on civilian side
		faction	= CIV_F;		/// and with civilian faction
	};
	// Derivate from the base class
};