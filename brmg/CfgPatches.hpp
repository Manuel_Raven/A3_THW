class CfgPatches
{
	class MNLRVN_GKW
	{
		// List of units defined in this "PBO"
		units[]= 
		{
			"C_MNLRVN_GKW"
		};
		weapons[]={};
		requiredVersion=0.1;
		// because we are using references to this addon, our test car should not be loaded if A3_Soft_F is not present
		requiredAddons[]	= {"A3_Soft_F"};
	};
};